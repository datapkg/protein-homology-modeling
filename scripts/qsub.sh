#!/bin/bash

set -ev

unset XDG_RUNTIME_DIR

# === Spark jobs ===
if [[ -n ${USE_SPARK} ]] ; then
    export SPARK_MASTER_HOST=$(hostname -f)
    export SPARK_IDENT_STRING=${SLURM_JOBID}
    export SPARK_WORKER_DIR=${SLURM_TMPDIR}
    export SPARK_LOG_DIR=${SCRATCH}/spark-logs
    mkdir -p ${SPARK_WORKER_DIR} ${SPARK_LOG_DIR}

    start-master.sh
    sleep 5

    (
    export SPARK_NO_DAEMONIZE=1;
    srun -x $(hostname -s) -n $((SLURM_NTASKS - 1)) --label \
        --output=${SPARK_LOG_DIR}/spark-${SPARK_IDENT_STRING}-workers.out \
        ${SPARK_HOME}/sbin/start-slave.sh spark://${SPARK_MASTER_HOST}:7077 \
        --cores ${SLURM_CPUS_PER_TASK} \
        --memory ${SLURM_MEM_PER_NODE}M
    ) &

    sleep 5
fi

# === Array jobs ===
EXTENSION=""
if [[ -n ${SLURM_ARRAY_TASK_ID} ]] ; then
    EXTENSION="_${SLURM_ARRAY_TASK_ID}"
fi

# === Run notebook ===

# TODO: Move this to a standalone package
python ${SLURM_SUBMIT_DIR}/scripts/execute_notebook.py \
    -i ./notebooks/${NOTEBOOK_NAME}.ipynb \
    -o "${OUTPUT_DIR}/${NOTEBOOK_NAME}${EXTENSION}.html"

# Alternatively, we can use the CLI
# jupyter nbconvert ./notebooks/${NOTEBOOK_NAME}.ipynb \
#     --to=html_ch \
#     --execute \
#     --allow-errors \
#     --output="${NOTEBOOK_NAME}${EXTENSION}.html" \
#     --output-dir="${OUTPUT_DIR}" \
#     --ExecutePreprocessor.timeout=$((60 * 60 * 24 * 7))
#     # --template=docs/output_toggle.tpl \

# === Cleanup ===
if [[ -n ${USE_SPARK} ]] ; then
    ${SPARK_HOME}/sbin/stop-master.sh
fi
