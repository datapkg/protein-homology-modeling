# protein-homology-modeling

[![docs](https://img.shields.io/badge/docs-v0.1-blue.svg)](https://datapkg.gitlab.io/protein-homology-modeling/)
[![build status](https://gitlab.com/datapkg/protein-homology-modeling/badges/master/build.svg)](https://gitlab.com/datapkg/protein-homology-modeling/commits/master/)

Notebooks for creating homology models of proteins and protein-protein interactions.

## Notebooks
